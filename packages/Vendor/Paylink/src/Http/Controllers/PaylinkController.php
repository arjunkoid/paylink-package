<?php
namespace Vendor\Paylink\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Vendor\Paylink\Http\Services\Fibonacci;
use Vendor\Paylink\Http\Services\FizzBuzz;
use Vendor\Paylink\Http\Services\SoapService;
use Illuminate\Support\Facades\Log;

class PaylinkController extends Controller {

    public function index()
    {
        return view('vendor::index');
    }

    public function fizzBuzz()
    {
        try {
            $fizzBuzzObj = new FizzBuzz();
            /**
             * Get Result of Fizz Buzz
             */
            $fizzBuzzObj->getResult(1, 20);
        } catch (\Exception $e) {
            Log::error($e);
        }

        exit;

    }

    public function fibonacci()
    {
        try {
            $fibObj = new Fibonacci();
            $n = 10;
            for ($i=0; $i< $n; $i++) {
                echo $fibObj->generate($i) . " ";
            }

        }catch (\Exception $e) {
            Log::error($e);
        }

        exit;

    }


    public function list()
    {
        $obj = new SoapService();
        $physician = $obj->getPhysician(1, null);

        return view('vendor::index' ,compact('physician'));
    }

}