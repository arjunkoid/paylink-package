<?php
namespace Vendor\Paylink\Http\Services;

use Vendor\Paylink\Http\Services\SoapClient;
use Vendor\Paylink\Http\Services\SoapServiceInterface;


class SoapService implements SoapServiceInterface {
    /**
     * @var \Vendor\Paylink\Http\Services\SoapClient
     */
    protected $soapClient;

    /**
     * SoapService constructor.
     */
    public function __construct()
    {
        $this->soapClient = new SoapClient();
    }

    /**
     * @param      $cmd
     * @param null $param
     *
     * @return mixed
     */
    public function getPhysician($cmd, $param = null)
    {
        $repsone = $this->soapClient->sendRequest($cmd, $param);

        return $repsone;
    }

    /**
     * @param      $cmd
     * @param null $param
     *
     * @return mixed
     */
    public function getPhysicianAndSpeciality($cmd, $param = null)
    {
        $repsone = $this->soapClient->sendRequest($cmd, $param);

        return $repsone;
    }

    /**
     * @param      $cmd
     * @param null $param
     *
     * @return mixed
     */
    public function getAllSpecialty($cmd, $param = null)
    {
        $repsone = $this->soapClient->sendRequest($cmd, $param);

        return $repsone;
    }
}