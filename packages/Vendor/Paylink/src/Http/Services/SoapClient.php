<?php
namespace Vendor\Paylink\Http\Services;

use Vendor\Paylink\Http\Services\SoapInterface;

/**
 * Class SoapClient
 * @package Vendor\Paylink\Http\Services
 */

class SoapClient implements  SoapInterface {
    /**
     * @param $cmd
     * @param $params
     *
     * @return mixed
     */
    public function sendRequest($cmd, $params)
    {
        $endPoint = config('paylink.soap_endpoint');

        $soapClient = new \SoapClient($endPoint);

        $response = $soapClient->requestXML(['CMD' => $cmd,'PARAMS' => $params]);

        return $response;

    }
}