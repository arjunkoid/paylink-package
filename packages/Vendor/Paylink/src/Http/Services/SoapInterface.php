<?php
namespace Vendor\Paylink\Http\Services;

/**
 * Interface SoapInterface
 * @package Vendor\Paylink\Http\Services
 */
interface SoapInterface {
    public function sendRequest($cmd, $param);
}