<?php
namespace Vendor\Paylink\Http\Services;

/**
 * Interface SoapServiceInterface
 * @package Vendor\Paylink\Http\Services
 */
Interface SoapServiceInterface {
    public function getPhysician($cmd);
    public function getPhysicianAndSpeciality($cmd);
    public function getAllSpecialty($cmd);
}