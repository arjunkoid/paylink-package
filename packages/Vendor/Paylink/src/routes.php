<?php
Route::group(['namespace' => 'Vendor\Paylink\Http\Controllers', 'middleware' => ['web']], function(){
    Route::get('list', 'PaylinkController@index');
    Route::get('fizzbuzz', 'PaylinkController@fizzBuzz');
    Route::get('fibonacci', 'PaylinkController@fibonacci');
    Route::get('physician', 'PaylinkController@list');
});