<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Soap\Request\GetConversionAmount;
use App\Soap\Response\GetConversionAmountResponse;

class HomeController extends Controller
{

    /**
     * SoapController constructor.
     *
     * @param SoapWrapper $soapWrapper
     */

    public function __construct()
    {
//        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('home');
    }
}
